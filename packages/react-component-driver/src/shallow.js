import recodr from './lib/index';
import * as shallowBackend from './lib/backends/shallow';

module.exports = recodr(shallowBackend);
